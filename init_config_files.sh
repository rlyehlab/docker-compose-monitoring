#!/usr/bin/env bash

## Prometheus
sudo docker create --name prome prom/prometheus \
    && sudo docker start prome \
    && sudo docker exec prome cat /etc/prometheus/prometheus.yml \
    | tee prometheus-new.yml \
    && sudo docker stop prome \
    && sudo docker rm prome \
    && sudo docker volume prune

## Alertmanager
sudo docker create --name alertmanager prom/alertmanager \
    && sudo docker start alertmanager \
    && sudo docker exec alertmanager cat /etc/alertmanager/alertmanager.yml \
    | tee alertmanager-new.yml \
    && sudo docker stop alertmanager \
    && sudo docker rm alertmanager \
    && sudo docker volume prune
