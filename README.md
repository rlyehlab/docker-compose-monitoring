# Monitoring stack

## Requirements
- docker >= 18
- docker-compose >= 1.24

## Steps

1. Run `init_config_files.sh` that produce `prometheus-new.yml` and
`alertmanager-new.yml`
1. Rename `prometheus-new.yml` to `prometheus.yml` and
`alertmanager-new.yml` to  `config.yml`
1. Copy and rename `nginx/conf.d/default.conf-example` to
`nginx/conf.d/default.conf`
1. Copy and rename `env` file to `.env`
1. Copy and rename `docker-compose-monitoring.yml` to `docker-compose.yml`
1. Run `setup.sh` to initialitation of stack over docker

## new configs

1. If you made changes to configs files run `new_config.sh` to apply them