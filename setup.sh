#!/usr/bin/env bash

# First Run
sudo docker-compose up --no-start

## Vars
prometheus_container="$(sudo docker-compose ps |grep prometheus \
    |cut -d' ' -f1 |sed 'N;s/\n//')"
alertmanager_container="$(sudo docker-compose ps|grep alertmanager \
    |cut -d' ' -f1 |sed 'N;s/\n//')"
nginx_container="$(sudo docker-compose ps |grep nginx|cut -d' ' -f1 \
    |sed 'N;s/\n//')"

## Prometheus
sudo chown 65534:65534 prometheus.yml
sudo chown 65534:65534 rules.yml

sudo docker cp ./prometheus.yml \
"${prometheus_container}:/config/prometheus.yml"
sudo docker cp ./rules.yml "${prometheus_container}:/config/rules.yml"

sudo chown "${USER}" prometheus.yml
sudo chown "${USER}" rules.yml

## Alertmanager
sudo chown 65534:65534 alertmanager.yml

sudo docker cp alertmanager.yml \
    "${alertmanager_container}:/etc/alertmanager/config.yml"

sudo chown "${USER}" alertmanager.yml

## Nginx
sudo docker cp nginx/nginx.conf "${nginx_container}:/etc/nginx/nginx.conf"

sudo docker cp nginx/ssl-common.conf \
    "${nginx_container}:/etc/nginx/ssl-common.conf"

sudo docker cp nginx/conf.d/default.conf \
    "${nginx_container}:/etc/nginx/conf.d/default.conf"

## Up & Fun!
sudo docker-compose up -d
